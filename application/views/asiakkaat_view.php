<h3>Asiakkaat</h3>
<?php print anchor('asiakas/lisaa','Lisää');?>
<table class="table">
    <tr>
        <th>ID</th>
        <th>Sukunimi</th>
        <th>Etunimi</th>
        <th>Lähiosoite</th>
        <th>Postitoimipaikka</th>
        <th>Postinumero</th>
        <th></th>
        <th></th>
    </tr>
<?php
foreach ($asiakkaat as $asiakas) {
    print "<tr>";
    print "<td>$asiakas->id</td>";
    print "<td>$asiakas->sukunimi</td>";
    print "<td>$asiakas->etunimi</td>";
    print "<td>$asiakas->lahiosoite</td>";
    print "<td>$asiakas->postitoimipaikka</td>";
    print "<td>$asiakas->postinumero</td>";
    print "<td>" . anchor("asiakas/poista/$asiakas->id","Poista") . "</td>";
    print "<td>" . anchor("asiakas/muokkaa/$asiakas->id","Muokkaa") . "</td>";
    print "</tr>";
    
}
?>
</table>