<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">        
        <h3>Kirjaudu</h3>
        <form role="form" method="post" action="<?php print site_url(). '/kayttaja/kirjaudu'?>">
            <div class="form-group">
                <label for="tunnus">Käyttäjätunnus:</label>
                <input type="email" name="tunnus" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" name="salasana" class="form-control">
            </div>
            <button class="btn btn-primary">Kirjaudu</button>
            <a class="btn btn-default" href="<?php print site_url() . '/kayttaja/rekisteroityminen'?>">
                Rekisteröidy
            </a>
        </form>
    </div>
</div>