<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Asiakasrekisteri</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="<?php print base_url() . '/css/style.css';?>" rel="stylesheet">
    </head>
    <body>
        <h1>Asiakasrekisteri</h1>
        <div class="container">
        <?php
        $this->load->view($main_content);
        ?>
        </div>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>
