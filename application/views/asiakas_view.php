<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h3>Lisää asiakas</h3>
        <?php
        print validation_errors("<p style='color: red'>","</p>");
        ?>
        <form action="<?php print site_url() . '/asiakas/tallenna';?>" method="post">
            <input type="hidden" name="id" value="<?php print $id;?>">
            <div class="form-group">
                <label>Sukunimi</label>
                <input name="sukunimi" class="form-control" value="<?php print $sukunimi;?>">
                <?php print form_error('sukunimi');?>
            </div>
            <div class="form-group">
                <label>Etunimi</label>
                <input name="etunimi" class="form-control" value="<?php print $etunimi;?>">
            </div>
            <div class="form-group">
                <label>Lähiosoite</label>
                <input name="lahiosoite" class="form-control" value="<?php print $lahiosoite;?>">
            </div class="form-group">
            <div class="form-group">
                <label>Postitoimipaikka</label>
                <input name="postitoimipaikka" class="form-control" value="<?php print $postitoimipaikka;?>">
            </div>
            <div class="form-group">
                <label>Postinumero</label>
                <input name="postinumero" class="form-control" value="<?php print $postinumero;?>">
            </div>
            <div>
                <button>Tallenna</button>
            </div>
        </form>
    </body>
</html>
