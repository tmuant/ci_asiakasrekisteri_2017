<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kayttaja extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('kayttaja_model');
        $this->load->library('encrypt');
        $this->load->library('form_validation');
    }
    
    public function index() {
        $data['main_content'] = 'kirjaudu_view';
        $this->load->view('template',$data);
    }
    
    public function rekisteroityminen() {
        $data['main_content'] = 'rekisteroidy_view';
        $this->load->view('template',$data);
    }   
    
    public function rekisteroidy() {
        $data = array(            
            'email' => $this->input->post('tunnus'),
            'salasana' => $this->encrypt->encode($this->input->post('salasana'))
        );
        
        $this->form_validation->set_rules('tunnus','tunnus','required|valid_email');
        $this->form_validation->set_rules('salasana','salasana','required|min_length[8]');
        $this->form_validation->set_rules('salasana2','salasana uudestaan','matches[salasana]');
        
        
        if ($this->form_validation->run()===TRUE) {                
            $this->kayttaja_model->lisaa($data);
            $this->index();
        }
        else {
            $data['main_content'] = 'rekisteroidy_view';
            $this->load->view('template',$data);
        }
    }
    
}











