/**/
drop database if exists asiakasrekisteri;

create database asiakasrekisteri;

use asiakasrekisteri;

create table asiakas(
    id int primary key auto_increment,
    etunimi varchar(50) not null,
    sukunimi varchar(50) not null,
    lahiosoite varchar(50),
    postitoimipaikka varchar(50),
    postinumero varchar(5)
);

create table kayttaja(
    id int primary key auto_increment,
    email varchar(100) not null unique,
    salasana varchar(255) not null
);


insert into asiakas(etunimi,sukunimi) values ('Jouni','Juntunen');
insert into asiakas(etunimi,sukunimi) values ('Testi','Henkilö');






